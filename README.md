# docker-with-cloud-sdk

This image is basically an extension of the [official docker image](https://hub.docker.com/_/docker) with the google
cloud sdk installed. This can be used to update the all kinds of google cloud platform infrastrcuture or getting
permissions to push to a private container registry.

## Usage

### in gitlab CI/CD pipeline

1. Retrieve access credentials: IAM -> Service Accounts -> choose your service account -> manage keys -> create json key
2. create a CI Variable in your gitlab project (e.g. GITLAB_SERVICE_ACCOUNT) and insert your json as string. You NEED to
   protect the variable, especially if you use shared runners. Read
   more [here](https://docs.gitlab.com/ee/ci/variables/README.html#protect-a-cicd-variable). If you use an public
   project, you should mask your variable as well. Pay attention to the regex to match the variable. You can base64
   encode your string and revert this in the "before-script" part if needed

3. add the following into your gitlab-ci.yml

```yaml
before_script:
  - echo $GCLOUD_SERVICE_KEY > ${HOME}/gcloud-service-key.json
  - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
  - docker login -u _json_key --password-stdin https://eu.gcr.io < ${HOME}/gcloud-service-key.json
```
